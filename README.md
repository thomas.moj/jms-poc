# JMS POC

Zwei Java EE Anwendungen, die über JMS miteinander kommunizieren können.
Das Projekt kann genutzt werden um zu testen, ob eine JMS Kommunikation zwischen zwei Java EE Appservern grundsätzlich möglich ist.
Getest wurde die Anwendung mit einem wildfly-15.0.1.Final

## Producer

Hier findet sich eine einfache JSF Anwendung mit der eine JMS Nachricht erzeugt werden kann.
Alle Befehle zum Einrichten von Netty und der JMS Queue finden sich unter:
producer/wildflyconfig/config.cli
Der Netty Connector wird auf Port 5445 eingrichtet. Das kann bei Bedarf in der config.cli auch angepasst werden.

Damit andere Appserver oder Clienten die Nachrichten abfragen können, muss ein entsprechender User eingerichtet werden:

```
add-user.sh -a -u appuser -p test123 -g guest
```

## Consumer

Hier findet sich eine einfache Message Driven Bean, die auf Nachrichten des Producer wartet und diese über System.out.println in das Severlog schreibt.
Alle Befehle zum Einrichten der Verbindung zum anderen Appserver finden sich unter:
consumer/wildflyconfig/config.cli

Ohne Änderungen würde die Verbindung auf localhost port 5445 eingerichtet. Das muss entsprechend angepasst in config.cli angepasst werden.