package de.tmoj.demo.javaee.jmspoc.producer;

import java.time.LocalDateTime;

import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.Session;

@Named
@RequestScoped
public class MessageProducer {

    private String message;
    private String userFeedback;
    @Resource(lookup = "java:/ConnectionFactory")
    private ConnectionFactory connectionFactory;
    @Resource(lookup = "java:jboss/exported/jms/queue/test")
    private Queue messageQueue;

    public String sendMessage() {
        try (Connection connection = connectionFactory.createConnection()) {
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            String msgWithTimestamp = LocalDateTime.now() + ": " + message;
            session.createProducer(messageQueue).send(session.createTextMessage(msgWithTimestamp));
            userFeedback = "Message Send: " + msgWithTimestamp;
        } catch (JMSException e) {
            e.printStackTrace();
            userFeedback = e.getMessage();
        }
        return "feedback";
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserFeedback() {
        return userFeedback;
    }

    public void setUserFeedback(String userFeedback) {
        this.userFeedback = userFeedback;
    }
}
